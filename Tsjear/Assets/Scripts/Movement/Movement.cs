﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{

    private float jumpPower = 80f;
    public float speed = 50f;
    public float maxSpeed = 50;
    private Rigidbody2D rb2d;
    public bool grounded;
    [SerializeField] 
    public float jumptimes = 1;
    private float MaxJumpTimes = 0;
    public float JumpHeight = 3;
    public float maxJumpHeight = 4;
    public bool _Jumping;
    public bool _inputJump = false;
    private Animator _anim;
    public float _JumpHeight = 1;
    protected bool wave;


    void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        _anim = gameObject.GetComponent<Animator>();
    }
        void Update()
    { 
        _anim.SetFloat("MoveSpeed", Mathf.Abs(Input.GetAxis("Horizontal")));
        _anim.SetBool("Grounded", grounded);
        _anim.SetBool("KeyPressed", wave);

        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            transform.localScale = new Vector3(-3, 3, 1);
        }

        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            transform.localScale = new Vector3(3, 3, 1);
        }

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            JumpFunction();
        }
            if (Input.GetKeyDown(KeyCode.O))
            {
               
                wave = true;

            }
            else
            {
                wave = false;
            }
            
    }

    void FixedUpdate()
    {
        // player movement
        float h = Input.GetAxis("Horizontal");
        rb2d.AddForce((Vector2.right * speed) * h);

        // Limit player speed
        if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
         
        }

        if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2(-maxSpeed, rb2d.velocity.y);
        }


    }


    public void JumpFunction()
    {

        if (jumptimes <= MaxJumpTimes)
        {
            Debug.Log("jump");
            grounded = false;

            if (!_inputJump)
            {
               
                JumpHeight = transform.position.y;
            }

            _Jumping = true;
            _inputJump = true;
            StartCoroutine("Jump");
        }
    }

    IEnumerator Jump()
    {

        while (true)
        {
            if (transform.position.y >= maxJumpHeight + JumpHeight)
            {
                _inputJump = false; jumptimes++; JumpHeight += maxJumpHeight + JumpHeight;
            }

            if (_inputJump)
            {
                _Jumping = true; transform.Translate(Vector3.up * jumpPower * Time.smoothDeltaTime);
            }
            else if (!_inputJump)
            {
                StopAllCoroutines();
            }
            yield return new WaitForEndOfFrame();
        }
    }  
        
}