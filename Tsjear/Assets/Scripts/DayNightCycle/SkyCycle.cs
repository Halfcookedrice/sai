﻿using UnityEngine;
using System;
using System.Collections;

public class SkyCycle : MonoBehaviour
{


    private int dayLength;   //in minutes
    private int dayStart;
    private int nightStart;   //also in minutes
    private int currentTime;
    public float cycleSpeed;
    private bool isDay;
    private Vector3 sunPosition;
    public GameObject sun;
    public GameObject earth;


    void Start()
    {
        dayLength = 1440;
        dayStart = 300;
        nightStart = 1200;
        currentTime = 720;
        StartCoroutine(TimeOfDay());
        earth = gameObject.transform.parent.gameObject;
    }

    void Update()
    {

        if (currentTime > 0 && currentTime < dayStart)
        {
            isDay = false;

        }
        else if (currentTime >= dayStart && currentTime < nightStart)
        {
            isDay = true;

        }
        else if (currentTime >= nightStart && currentTime < dayLength)
        {
            isDay = false;

        }
        else if (currentTime >= dayLength)
        {
            currentTime = 0;
        }
        float currentTimeF = currentTime;
        float dayLengthF = dayLength;
        earth.transform.eulerAngles = new Vector3(0, 0, (-(currentTimeF / dayLengthF) * 360) + 90);
    }

    IEnumerator TimeOfDay()
    {
        while (true)
        {
            currentTime += 1;
            int hours = Mathf.RoundToInt(currentTime / 60);
            int minutes = currentTime % 60;
          //  Debug.Log(hours + ":" + minutes);
            yield return new WaitForSeconds(1F / cycleSpeed);
        }
    }
}
