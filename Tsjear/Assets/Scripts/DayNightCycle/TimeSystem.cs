﻿using UnityEngine;
using System;
using System.Collections;

public class TimeSystem: MonoBehaviour {

     public Light Daylight;
     public int Hours { get; set; }
     public int Night = 18;
     public int Day = 6;
     public int hour;

    public float fadeSpeed = 2f;
    public float highIntensity = 8f;
    public float lowIntensity = 0f;
    public float changeMargin = 0.0001f;
    private float NightIntensity;
    private float DayIntensity;
    

    void Awake()
    {
        Daylight.intensity = 0.4f;
        DayIntensity = highIntensity;
        NightIntensity = lowIntensity;
         
    }
    void OnGUI()
    {   
        DateTime time = DateTime.Now;
        hour = time.Hour;
        int minute = time.Minute;
        int second = time.Second;

        GUI.contentColor = Color.black;
        GUILayout.Label(hour + ":" + minute + ":" + second);

        if (hour >= Night)
        {
            Daylight.intensity = Mathf.Lerp(Daylight.intensity, NightIntensity, fadeSpeed * Time.deltaTime);
            
        }
        else if (hour >= Day)
        {
            Daylight.intensity = Mathf.Lerp(Daylight.intensity, DayIntensity, fadeSpeed * Time.deltaTime);
            
        }

    }



}
