using UnityEngine;
using System.Collections;

public class TransitionCamera : MonoBehaviour
{
    
    //Attached to the bonus blocks

    public static PlayerCam playerCam;
    public static SkyCam skyCam;

    public bool touching;
    protected Animator _anim;



    public static float timer = .1f;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") // If colliding with player, execute this block
        {

            _anim.SetBool("Touched", touching);
         

            CameraGo();//Make the camera go to the target if the player collides with this
        }
    }

    void Start()
    {
        _anim = gameObject.GetComponent<Animator>();
        playerCam = FindObjectOfType<PlayerCam>();
        skyCam = FindObjectOfType<SkyCam>();
     
    }

    void CameraGo()//turning the skycam on so it pans over to the target and playercam off so it stops following the player
    {
        playerCam.enabled = false;
        skyCam.enabled = true;
    }

    public static void CameraReturn()//turns skycam off so it and playercam on so the camera goes back to the player
    {
        skyCam.enabled = false;
        playerCam.enabled = true;
    }
}
