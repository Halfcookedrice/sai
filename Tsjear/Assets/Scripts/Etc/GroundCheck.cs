﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {
    private Movement player;	 
	void Start () 
    {
        player = gameObject.GetComponentInParent<Movement>();
	}

    public void OnTriggerEnter2D(Collider2D col)
    {
        player.grounded = true;
   
        player.jumptimes = 0;
        player.JumpHeight = transform.position.y;
    }
    public void OnTriggerStay2D(Collider2D col)
    {
        player.grounded = true;
     
        player.jumptimes = 0;
        player.JumpHeight = transform.position.y;
        
    }
    public void onTriggerExit(Collider2D col)
    {
        player.grounded = false;
       
    }
}
