﻿using UnityEngine;
using Newtonsoft.Json;
using System.Collections;
using System;


public class GetURL : MonoBehaviour
{
    private static string _jsonData;
    private const string ApiKey = @"d33ba8071e6bde8b";  
    private const string WebAPIUrl = @"http://api.wunderground.com/api/d33ba8071e6bde8b/forecast/q/autoip.json";     
    public string stringTemp;
    public int floatTemp;
    public int ChanceofRain;
    public string locationAccuracy;
    public float warm = 20;
    public float cold = 11;
    public float freezing = 0;     

    public void Start()
    {
        // Deserialize json format into a c# model format
        _jsonData = getJsonDataFromAPI(WebAPIUrl);
        var weatherData = JsonConvert.DeserializeObject<WeatherData>(_jsonData);
                           
            Debug.Log(_jsonData);                  
          
            if (weatherData != null && weatherData.forecast != null)
        // Here is the access to sub-properties of the the model
        {
            for (int i = 0; i < weatherData.forecast.simpleforecast.forecastday.Length; i++)
                stringTemp = weatherData.forecast.simpleforecast.forecastday[0].high.celsius;
                ChanceofRain = weatherData.forecast.simpleforecast.forecastday[0].pop;
                locationAccuracy = weatherData.forecast.simpleforecast.forecastday[0].date.tz_long;
             
                if (int.TryParse(stringTemp, out floatTemp ))
               
                // console display of weather goes here
                {
                Debug.Log("Current °C  " + floatTemp);
                Debug.Log("There is a  " + ChanceofRain + " %  " + "chance of rain");
                Debug.Log("Location accuracy: " + locationAccuracy);
                }
        }

        
            // whether a jacket is likely needed or not
        if (floatTemp <= cold)
        {
            Debug.Log("Het is koud.");          
        }
        if (floatTemp <= freezing)
        {
            Debug.Log("Het vriest!");
        }
        if (floatTemp >= cold)
        {
            Debug.Log("Het is een beetje fris!");        
        }
        else if (floatTemp >= warm)
        {
            Debug.Log("Het is warm buiten!");
             
        }
        
    }

    public static string getJsonDataFromAPI(string websiteUrl)
    {
        using (var webClient = new System.Net.WebClient())
        {
            return webClient.DownloadString(websiteUrl);
        }
    }        
}

// This is the parent class

public class WeatherData
{
    public Response response { get; set; }    
    public Forecast forecast { get; set; }
}

public class Response
{
    public string version { get; set; }
    public string termsofService { get; set; }
    public Features features { get; set; }     
}

public class Features
{
    
    public int forecast { get; set; }
}


public class Forecast
{
    public Txt_Forecast txt_forecast { get; set; }
    public Simpleforecast simpleforecast { get; set; }
}
public class Txt_Forecast
{
    public string date { get; set; }
    public Forecastday[] forecastday { get; set; }
}
public class Forecastday
{
    public int period { get; set; }
    public string icon { get; set; }
    public string icon_url { get; set; }
    public string title { get; set; }
    public string fcttext { get; set; }
    public string fcttext_metric { get; set; }
    public string pop { get; set; }
}
public class Simpleforecast
{
    public Forecastday1[] forecastday { get; set; }
}
  
public class Forecastday1
{
    public Date date { get; set; }
    public int period { get; set; }
    public High high { get; set; }
    public Low low { get; set; }
    public string conditions { get; set; }
    public string icon { get; set; }
    public string icon_url { get; set; }
    public string skyicon { get; set; }
    public int pop { get; set; }
    public Qpf_Allday qpf_allday { get; set; }
    public Qpf_Day qpf_day { get; set; }
    public Qpf_Night qpf_night { get; set; }
    public Snow_Allday snow_allday { get; set; }
    public Snow_Day snow_day { get; set; }
    public Snow_Night snow_night { get; set; }
    public Maxwind maxwind { get; set; }
    public Avewind avewind { get; set; }
    public int avehumidity { get; set; }
    public int maxhumidity { get; set; }
    public int minhumidity { get; set; }
}
public class Date
{
    public string epoch { get; set; }
    public string pretty { get; set; }
    public int day { get; set; }
    public int month { get; set; }
    public int year { get; set; }
    public int yday { get; set; }
    public int hour { get; set; }
    public string min { get; set; }
    public int sec { get; set; }
    public string isdst { get; set; }
    public string monthname { get; set; }
    public string monthname_short { get; set; }
    public string weekday_short { get; set; }
    public string weekday { get; set; }
    public string ampm { get; set; }
    public string tz_short { get; set; }
    public string tz_long { get; set; }
}

public class High
{
    public string fahrenheit { get; set; }
    public string celsius { get; set; }
}
public class Low
{
    public string fahrenheit { get; set; }
    public string celsius { get; set; }
}
public class Qpf_Allday
{
    public float _in { get; set; }
    public int mm { get; set; }
}
public class Qpf_Day
{
    public float? _in { get; set; }
    public int? mm { get; set; }
}
public class Qpf_Night
{
    public float _in { get; set; }
    public int mm { get; set; }
}
public class Snow_Allday
{
    public float _in { get; set; }
    public float cm { get; set; }
}
public class Snow_Day
{
    public float? _in { get; set; }
    public float? cm { get; set; }
}
public class Snow_Night
{
    public float _in { get; set; }
    public float cm { get; set; }
}
public class Maxwind
{
    public int mph { get; set; }
    public int kph { get; set; }
    public string dir { get; set; }
    public int degrees { get; set; }
}
public class Avewind
{
    public int mph { get; set; }
    public int kph { get; set; }
    public string dir { get; set; }
    public int degrees { get; set; }
}
 