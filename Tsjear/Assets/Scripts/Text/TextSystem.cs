using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextSystem : MonoBehaviour {//Attached to the UI Text under the Canvas

    private static string text;//String text that will hold all the text
    private static bool trigger;//a simple bool trigger
    private Text TextComponent;//variable that will be used to control the .Text part of this object
    private int i = 0;//nested int i to control the loop
    private string letter = "";//string to display each letter after another

    void Start()
    {
        TextComponent = GetComponent<Text>();//get the Text component of this object
        trigger = false;//making trigger equal default false
    }


    public static void SetText(string targetText)
    {
       text = targetText;//Make the text that's goes into this function equal to our text variable
       trigger = true;//this has been triggered so turn this on
    }

    void FixedUpdate()
    {
        if (trigger == true)//if we have recieved the text
        {
            StartCoroutine("TypeText");//start the coroutine
            trigger = false;//turn this off so we can use it again
        }
    }

    IEnumerator TypeText()
    {
        yield return new WaitForSeconds(0.2f);//wait .02s before you start displaying the text

        while (i < text.Length)//if the controller 'i' is less than the array of char (string) then do this
        {
            TextComponent.text = "";//make the text blank on the object
            letter += text[i];//Our holder equals the current 'i' value
            TextComponent.text = letter;//display the text that the holder contains
            i++;//increase 'i' for the next character
            yield return new WaitForSeconds(0.05f);//wait 1 second before another character shows up
        }

        if (i == text.Length)//if you hit the end of the text
        {
            i = 0;//reset 'i' for future use
            letter = "";//reset letter for future use

            TextTrigger.FinishTyping = true;//You finished typing so turn it on
            Invoke("CameraCall", TransitionCamera.timer + .4f);//Call this function in X amount of time
            GetComponent<TextSystem>().enabled = false;//Turn off this script
        }
    }

    void CameraCall()
    {
        TextComponent.text = "";//Make the text component empty
        SkyCam.TriggeredOnce = false;//reset the trigger on skycam
        TransitionCamera.CameraReturn();//Make the camera return to the player
    }
}
