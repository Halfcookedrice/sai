using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class TextTrigger : MonoBehaviour//Attach this to the bonus box
{
    public GameObject UIText;//make this equal the UI Text component under Canvas
    public TextAsset textFile;//Target the text file you want to be displayed
    private string text;//string for the text that will be shown
    private TextSystem script;//a TextSystem variable called script
    public Transform Camerastarget;//The target of the camera
    public static bool FinishTyping = false;//is it finish typing
    private bool ThisScriptTrigger = false;//prevents all copies of this script from triggering

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Movement>())//if something collides with this that has the Movement Script
        {
            SkyCam.targetText = Camerastarget;//Make SkyCam's target equal to this script's Camera Target

            text = textFile.text;//the string text now equals the string in the textFile
            if (text == "") print("Text not found");//if the text doesn't equal anything

            UIText.GetComponent<TextSystem>().enabled = true;//Enable the script that will eventually display the Text

            ThisScriptTrigger = true;//Make it only trigger once

            this.GetComponent<CircleCollider2D>().isTrigger = false;//Disable the Triggering on the cricle collider so it can't be trigged again
        }
    }

    void Update()
    {
        if (ThisScriptTrigger == true && SkyCam.destination == true)//If the camera reached it's destination and this only triggered once
        {
            TextSystem.SetText(text);//Tell the script to start displaying the text
            ThisScriptTrigger = false;//Reset the trigger so it can be used again
            SkyCam.destination = false;//reset the destination so it can be used again
        }
    }
}

