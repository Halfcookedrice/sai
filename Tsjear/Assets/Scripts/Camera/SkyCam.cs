﻿using UnityEngine;
using System.Collections;

public class SkyCam : MonoBehaviour{


    public static Transform targetText;//The transform's location of where you want the camera to go to
    public static bool destination = false;//Has the camera hit the target's location?
    public static bool TriggeredOnce = false;//making sure it only does it once

    Camera myCam;

    void Start()
    {
        myCam = gameObject.GetComponent<Camera>();
    }

    void Update()
    {
        myCam.orthographicSize = (Screen.height / 100f) / 0.3f;
        if (targetText && TriggeredOnce == false)//if you have a target and you've only done this once
        {
            LerpCamera();//pan the camera over
        }
        if (Mathf.Abs(Vector2.Distance(transform.position, targetText.position)) <= 2f && TriggeredOnce == false)//Testing to see if they camera's position is close the position of the target
        {
            TriggeredOnce = true;//turning this on so they only do this once
            destination = true;//They have hit the target's destination
        }
    }

    void LerpCamera()
    {
        transform.position = Vector3.Lerp(transform.position, targetText.position + new Vector3(0, 1, -10), TransitionCamera.timer);
    }
}

