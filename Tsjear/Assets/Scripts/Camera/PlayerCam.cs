﻿using UnityEngine;
using System.Collections;

public class PlayerCam : MonoBehaviour {
    public Transform target;
    Camera myCam;    

    void Start()
    {
        myCam = gameObject.GetComponent<Camera>();
    }   
    void Update()
    {
        myCam.orthographicSize = (Screen.height / 25f) / 0.1f;
        if (target)
            transform.position = Vector3.Lerp(transform.position, target.transform.position, 0.1f) + new Vector3(0, 5, -20);   
    }
}
